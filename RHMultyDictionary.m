//
//  RHMultyDictionary.m
//  RHString
//
//  Created by Сергей Иванушкин on 28.01.13.
//  Copyright (c) 2013 Сергей Иванушкин. All rights reserved.
//

#import "RHMultyDictionary.h"

@implementation RHMultyDictionary

- (id)init
{
    self = [super init];
    
    if(self){
        _dictionary = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (void)addObject:(id)object forKey:(id<NSCopying>)key
{
    NSMutableArray* array = _dictionary[key];
    
    if(!array){
        array = [[NSMutableArray alloc] initWithCapacity:1];
        
        [_dictionary setObject:array forKey:key];
    }
    
    NSAssert(array, @"Array must exist at this point");
    
    [array addObject:object];
}

- (void)removeLastObjectForKey:(id<NSCopying>)key;
{
    NSMutableArray* array = _dictionary[key];
    
    [array removeLastObject];
    
    if(![array count]){
        [_dictionary removeObjectForKey:key];
    }
}

- (NSArray*)objectsForKey:(id<NSCopying>)key
{
    return _dictionary[key];
}

- (id)lastObjectForKey:(id<NSCopying>)key
{
    return [_dictionary[key] lastObject];
}

- (NSArray*)allKeys
{
    return [_dictionary allKeys];
}

@end
