//
//  RHUIButton.m
//  RHString
//
//  Created by Сергей Иванушкин on 28.01.13.
//  Copyright (c) 2013 Сергей Иванушкин. All rights reserved.
//

#import "RHUIButton.h"
#import "RHStringParser.h"

@implementation RHUIButton

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self){
        NSAttributedString* text = [RHStringParser attributedStringFromRHString:[self titleForState:UIControlStateNormal]
                                                                          error:nil];
        if(text){
            [self setAttributedTitle:text forState:UIControlStateNormal];
        }
        
        text = [RHStringParser attributedStringFromRHString:[self titleForState:UIControlStateHighlighted]
                                                      error:nil];
        if(text){
            [self setAttributedTitle:text forState:UIControlStateHighlighted];
        }
        
        text = [RHStringParser attributedStringFromRHString:[self titleForState:UIControlStateDisabled]
                                                      error:nil];
        if(text){
            [self setAttributedTitle:text forState:UIControlStateDisabled];
        }
        
        text = [RHStringParser attributedStringFromRHString:[self titleForState:UIControlStateSelected]
                                                      error:nil];
        if(text){
            [self setAttributedTitle:text forState:UIControlStateSelected];
        }
    }
    
    return self;
}

@end
