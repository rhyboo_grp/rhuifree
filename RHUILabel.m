//
//  RHUILabel.m
//  RHString
//
//  Created by Сергей Иванушкин on 28.01.13.
//  Copyright (c) 2013 Сергей Иванушкин. All rights reserved.
//

#import "RHUILabel.h"
#import "RHStringParser.h"

@implementation RHUILabel

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self){
        NSAttributedString* text = [RHStringParser attributedStringFromRHString:self.text
                                                                          error:nil];
        
        if(text){
            self.attributedText = text;
        }
    }
    
    return self;
}

@end
