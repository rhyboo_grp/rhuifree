//
//  RHStringParser.h
//  RHString
//
//  Created by Сергей Иванушкин on 28.01.13.
//  Copyright (c) 2013 Сергей Иванушкин. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RHMultyDictionary.h"

@interface RHStringParser : NSObject<NSXMLParserDelegate>
{
    NSXMLParser* _parser;
    
    NSMutableAttributedString* _result;
    
    RHMultyDictionary* _dictionary;
}

+(NSAttributedString*)attributedStringFromRHString:(NSString*)string error:(NSError**)error;

@end
