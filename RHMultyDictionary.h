//
//  RHMultyDictionary.h
//  RHString
//
//  Created by Сергей Иванушкин on 28.01.13.
//  Copyright (c) 2013 Сергей Иванушкин. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RHMultyDictionary : NSObject
{
    NSMutableDictionary* _dictionary;
}

- (void)addObject:(id)object forKey:(id<NSCopying>)key;

- (void)removeLastObjectForKey:(id<NSCopying>)key;

- (NSArray*)objectsForKey:(id<NSCopying>)key;
- (id)lastObjectForKey:(id<NSCopying>)key;

- (NSArray*)allKeys;

@end
