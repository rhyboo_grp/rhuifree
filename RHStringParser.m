//
//  RHStringParser.m
//  RHString
//
//  Created by Сергей Иванушкин on 28.01.13.
//  Copyright (c) 2013 Сергей Иванушкин. All rights reserved.
//

#import "RHStringParser.h"

static NSString* kRHStr             = @"rh_str";

static NSString* kRHFont            = @"font";
static NSString* kRHColor           = @"color";
static NSString* kRHBackground      = @"background";

static NSString* kRHName            = @"name";
static NSString* kRHSize            = @"size";

static NSString* kRHRed             = @"r";
static NSString* kRHGreen           = @"g";
static NSString* kRHBlue            = @"b";
static NSString* kRHAlpha           = @"a";

static NSString* kRHSystem          = @":system:";
static NSString* kRHSystemBold      = @":systemBold:";
static NSString* kRHSystemItalic    = @":systemItalic:";

@interface RHStringParser ()

- (id)initWithRHString:(NSString*)string;
- (BOOL)parse;
- (NSError*)error;

- (NSAttributedString*)result;

@end

@interface RHStringParser (Helpers)

- (UIFont*)fontFromDictionary:(NSDictionary*)dictionary;
- (UIColor*)colorFromDictionary:(NSDictionary*)dictionary;

@end

@implementation RHStringParser

+(NSAttributedString*)attributedStringFromRHString:(NSString*)string error:(NSError**)error
{
    RHStringParser* parser = [[RHStringParser alloc] initWithRHString:string];
    
    if(![parser parse]){
        if(error){
            *error = [parser error];
        }
        
        return nil;
    }
    
    if(error){
        *error = [NSError errorWithDomain:@"RHString parser" code:0 userInfo:nil];
    }
    
    return [parser result];
}

- (id)initWithRHString:(NSString*)string
{
    self = [super init];
    
    if(self){
        _parser = [[NSXMLParser alloc] initWithData:[string dataUsingEncoding:NSUTF8StringEncoding]];
        _parser.delegate = self;
    }
    
    return self;
}

- (BOOL)parse
{
    return [_parser parse];
}

- (NSError*)error
{
    return [_parser parserError];
}

- (NSAttributedString*)result
{
    return _result;
}

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:kRHStr]){
        if(_dictionary){
            [parser abortParsing];
            return;
        }
        
        if(_result){
            [parser abortParsing];
            return;
        }
        
        _dictionary = [[RHMultyDictionary alloc] init];
        _result = [[NSMutableAttributedString alloc] init];
        
        return;
    }
    
    if([elementName isEqualToString:kRHFont]){
        if(!_dictionary){
            [parser abortParsing];
            return;
        }
        
        UIFont* font = [self fontFromDictionary:attributeDict];
        
        if(!font){
            [parser abortParsing];
            return;
        }
        
        [_dictionary addObject:font forKey:NSFontAttributeName];
        
        return;
    }
    
    if([elementName isEqualToString:kRHColor]){
        if(!_dictionary){
            [parser abortParsing];
            return;
        }
        
        UIColor* color = [self colorFromDictionary:attributeDict];
        
        if(!color){
            [parser abortParsing];
            return;
        }
        
        [_dictionary addObject:color forKey:NSForegroundColorAttributeName];
        
        return;
    }
    
    if([elementName isEqualToString:kRHBackground]){
        if(!_dictionary){
            [parser abortParsing];
            return;
        }
        
        UIColor* color = [self colorFromDictionary:attributeDict];
        
        if(!color){
            [parser abortParsing];
            return;
        }
        
        [_dictionary addObject:color forKey:NSBackgroundColorAttributeName];
        
        return;
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:kRHStr]){
        _dictionary = nil;
    }
    
    if([elementName isEqualToString:kRHFont]){
        [_dictionary removeLastObjectForKey:NSFontAttributeName];
    }
    
    if([elementName isEqualToString:kRHColor]){
        [_dictionary removeLastObjectForKey:NSForegroundColorAttributeName];
    }
    
    if([elementName isEqualToString:kRHBackground]){
        [_dictionary removeLastObjectForKey:NSBackgroundColorAttributeName];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSArray* keys = [_dictionary allKeys];
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    
    for(NSString* str in keys){
        [dict setObject:[_dictionary lastObjectForKey:str]
                 forKey:str];
    }
    
    NSAttributedString* attrString = [[NSAttributedString alloc] initWithString:string
                                                                     attributes:dict];

    [_result appendAttributedString:attrString];
}

@end

@implementation RHStringParser (Helpers)

- (UIFont*)fontFromDictionary:(NSDictionary*)dictionary
{
    if(!dictionary[kRHName]){
        return nil;
    }
    
    if(!dictionary[kRHSize]){
        return nil;
    }
    
    NSString* fontName = dictionary[kRHName];
    float size = [dictionary[kRHSize] floatValue];
    
    if([fontName isEqualToString:kRHSystem]){
        return [UIFont systemFontOfSize:size];
    }
    
    if([fontName isEqualToString:kRHSystemBold]){
        return [UIFont boldSystemFontOfSize:size];
    }
    
    if([fontName isEqualToString:kRHSystemItalic]){
        return [UIFont italicSystemFontOfSize:size];
    }
    
    return [UIFont fontWithName:fontName size:size];
}

- (UIColor*)colorFromDictionary:(NSDictionary*)dictionary
{
    if(!dictionary[kRHRed]){
        return nil;
    }
    
    if(!dictionary[kRHGreen]){
        return nil;
    }
    
    if(!dictionary[kRHBlue]){
        return nil;
    }
    
    float red = [dictionary[kRHRed] floatValue];
    float green = [dictionary[kRHGreen] floatValue];
    float blue = [dictionary[kRHBlue] floatValue];
    
    float alpha = 1;
    
    if(dictionary[kRHAlpha]){
        alpha = [dictionary[kRHAlpha] floatValue];
    }
    
    return [UIColor colorWithRed:red
                           green:green
                            blue:blue
                           alpha:alpha];
}

@end
